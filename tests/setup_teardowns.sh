setup_default_location() {
    # Install pp to its default location.
    #
    # We're intentionally avoiding using PP_ROOT as the name of the variable
    # defining the location, because that's a pp configuration variable. In this
    # file, we want to test pp's behavior in the default case without setting
    # any of the configuration variables.
    TESTING_PP_ROOT=~/.pp
    mkdir -p $TESTING_PP_ROOT
    cp -r /code/lib $TESTING_PP_ROOT
    source $TESTING_PP_ROOT/lib/pp.bash

    # Create the default location for virtual environments.
    TESTING_PP_ENV_ROOT=$TESTING_PP_ROOT/envs
    mkdir -p $TESTING_PP_ENV_ROOT

    # Set up a mock pyenv installation in its default location.
    TESTING_PYENV_ROOT=~/.pyenv
    TESTING_PP_PYTHON_ROOT=$TESTING_PYENV_ROOT/versions
    mkdir -p $TESTING_PP_PYTHON_ROOT
}


setup_nondefault_location() {
    # Install pp to a non-default location, using the pp configuration variable
    # PP_ROOT to define the location.
    TESTING_PP_ROOT=~/opt/pp
    mkdir -p $TESTING_PP_ROOT
    cp -r /code/lib $TESTING_PP_ROOT
    source $TESTING_PP_ROOT/lib/pp.bash
    PP_ROOT=$TESTING_PP_ROOT

    # Use a non-default location for the virtual environments, using the pp
    # configuration variable PP_ENV_ROOT to define the location..
    TESTING_PP_ENV_ROOT=~/venvs
    mkdir -p "$TESTING_PP_ENV_ROOT"
    PP_ENV_ROOT=$TESTING_PP_ENV_ROOT

    # Set up a mock pyenv installation in a non-default location, using the pp
    # configuration variable PP_PYTHON_ROOT to define the location.
    TESTING_PYENV_ROOT=~/.pyenv
    TESTING_PP_PYTHON_ROOT=$TESTING_PYENV_ROOT/versions
    mkdir -p $TESTING_PP_PYTHON_ROOT
    PP_PYTHON_ROOT=$TESTING_PP_PYTHON_ROOT
}


teardown_installation_paths() {
    rm -rf "$TESTING_PP_ENV_ROOT"
    rm -rf "$TESTING_PP_PYTHON_ROOT"
    rm -rf "$TESTING_PP_ROOT"
    rm -rf "$TESTING_PYENV_ROOT"
}
