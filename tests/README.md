# Tests

The test suite for pp is implemented as a series of Bash functions and a script to run them. Its design was inspired by test suites used for other shell projects and by test frameworks. We decided to roll our own instead of using an existing test framework, because it was simple enough that taking on an external dependency didn't seem worthwhile.

You are **strongly encouraged** to run the tests using Docker, because the tests may delete all your locally-installed virtual environments and Python installations if you run them on your machine directly.

## Running the tests

First build the Docker image with:

```
docker-compose build
```

Then run the full test suite with:

```
docker-compose run --rm tester
```

Or you can run a single test by passing its name as an argument to the `run_tests.sh` script. For example, if the only test you want to run is the one defined in the function `test_has_help()`, then use the following command:

```
docker-compose run --rm tester ./run_tests.sh has_help
```

## Writing a test

This section describes several points to understand when writing a test.

### Test file

Each test must be defined in a test file. The test file must be named like `test_*.sh`. For example, the file [test_core.sh](./test_core.sh) includes several tests.

### Test name

Each test has a name. The name will be printed in the test report along with a pass/fail status, so it should give some indication of what the test does. For example, the test verifying that the `--help` option is available is named `has_help`.

### Test function

Each test must be defined as a function in a test file. The function must be named as `test_<name>` to be detected by the test runner. For example, the function for the `has_help` test should be named `test_has_help`.

The body of the test function should be written such that its return code indicates whether the test passed or failed. In other words, the test passes if the function returns 0, and the test fails if the function returns non-0. Any Bash construct is available within the test.

### Setup and teardown

Two special functions are required to be defined in each test file (or at least sourced from another file): `setup` and `teardown`. `setup` will be run before every test in the file, and `teardown` will be run after every test in the file.

If you don't need to run any setup or teardown commands, you at least need to define them as empty functions:

```
setup() {
    :  # no-op
}
teardown() {
    :  # no-op
}
```

### Mocks

pp performs some of its tasks by delegating to other commands assumed to be present on the system, such as `pyenv` or `activate`. For the tests, we generally don't want to call the real versions of those commands, because:

- They may be expensive. For example, `pyenv install` could take a couple minutes to run, because it downloads and compiles Python. Lots of our tests run `pp py install`, which delegates to `pyenv install`, so running the real version could take a long time.
- They require extra installation steps and dependencies. We could certainly set these up if we had to, but it would be nice not to do that if possible.

To avoid calling the real versions, we inject mock versions into the test environment. The mocks should do just enough to meet pp's expectations of them. For example, after installing a new Python version, pp expects to find a directory for that Python version, and it expects to be able to run the `bin/python` executable in that directory. Therefore, the mock version of `pyenv install` creates a directory and adds a `bin/python` script to it. (The `bin/python` script itself is also a mock that supports the `-m venv my-env` arguments to create a virtual environment.)

The test runner doesn't inject the mocks into the environment automatically. They must be injected either by the `setup` function or by the test function itself.

## Anatomy of the test runner

Review [run_tests.sh](./run_tests.sh) to see exactly how the test runner works, but here is a summary:

- Find all the test functions (the functions named like `test_*`) in all the test files (the files named like `test_*.sh`).
- Randomize the order of the tests within each test file.
- For each test function in each test file:
  - Perform the following inside a subshell:
    - Source the test file.
    - Run the `setup` function.
    - Run the test function and store its return code.
    - Run the `teardown` function.
  - Report a passing test if the test function returned successfully and a failing test otherwise.

Note that the test file is sourced for every test function in the file. For example, if a file contains 12 tests, it will be sourced 12 times. Because the files may be sourced lots of times, you probably shouldn't put anything in the test file that would be expensive when sourcing it. In other words, it's best to define everything in functions that can be called only when needed.
