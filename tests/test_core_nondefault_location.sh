# Test the scenario where pp and pyenv have been installed to non-default
# locations.


setup() {
    source setup_teardowns.sh
    setup_nondefault_location

    source mocks/functions.sh
}


teardown() {
    teardown_installation_paths
}


source core_tests.sh
