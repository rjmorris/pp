#!/bin/bash


# If a command-line argument is given, treat it as the name of a test, and run
# only that test. The name of the test doesn't include the "test_" prefix.
match_test_name=$1


find_test_names() {
    # Output a newline-separated list of test names found in the given file.
    #
    # A test is included if its name starts with "test_".
    #
    # The "test_" prefix in the function name is removed to form the test name.
    # For example, a function named "test_has_help" becomes a test named
    # "has_help".

    local test_file=$1

    # Use a subshell to avoid altering the current environment. Notably, we'll
    # be removing all functions that were defined prior to now, so we don't want
    # to do that in the current environment.
    (
        # To find the functions defined in the test file, the approach we're
        # using is:
        #
        #   1. Remove all existing functions.
        #   2. Source the test file.
        #   3. Find the list of defined functions.
        #
        # If we don't remove all existing functions first, we won't know whether
        # the functions available after sourcing the test file were defined in
        # the test file itself or were already defined prior to sourcing the
        # test file.

        local existing_func
        for existing_func in $(compgen -A function); do
            unset -f $existing_func
        done

        source $test_file

        local test_funcs
        test_funcs=$(compgen -A function | grep "^test_")

        echo "$test_funcs" | sed "s/^test_\([a-zA-Z0-9_]*\).*$/\1/"
    )
}


run_test() {
    # Run a test, bracketed by the setup and teardown functions, and return the
    # test's exit code.
    #
    # To keep the test isolated, call this function in a subshell. This function
    # will source the test file, so running it in a subshell will prevent
    # anything in the test file from leaking into the calling environment.

    local test_file=$1
    local test_name=$2

    source $test_file
    trap 'teardown' EXIT
    setup
    test_$test_name
    status=$?
    trap - EXIT
    teardown
    return $status
}


# Phase 1: Test discovery
#
# Find all the tests and store them in a data structure. The data structure is
# an associative array where:
#
#  - Each key is a file name containing tests.
#  - Each key's value is a list of test names found in that file.
#
# The order of the tests are sorted randomly to avoid any order dependencies in
# the tests.

declare -A discovered

for test_file in test_*.sh; do
    discovered[$test_file]=$(find_test_names $test_file | shuf)
done


# Phase 2: Test execution

overall_count=0
overall_pass=0

for test_file in "${!discovered[@]}"; do
    echo $test_file:

    test_names=${discovered[$test_file]}
    for test_name in $test_names; do
        if [[ -n $match_test_name && $match_test_name != $test_name ]]; then
            continue
        fi

        (( overall_count++ ))

        # Run the test in a subshell to avoid altering the current environment.
        ( run_test $test_file $test_name )
        status=$?

        if [[ $status == 0 ]]; then
            (( overall_pass++ ))
            echo " pass: $test_name"
        else
            echo " FAIL: $test_name"
        fi
    done
done

echo $overall_pass/$overall_count tests passed.

[[ $overall_pass == $overall_count ]]
