setup() {
    # Install pp to its default location.
    ROOT_DIR=~/.pp
    mkdir -p $ROOT_DIR
    cp -r /code/lib $ROOT_DIR
    source $ROOT_DIR/lib/pp.bash

    # Create the default location for virtual environments.
    mkdir -p $ROOT_DIR/envs

    # Intentionally omit a pyenv installation by not creating its root directory
    # and not sourcing the mock pyenv function.
}


teardown() {
    rm -rf "$ROOT_DIR"
}


test_has_help() {
    # Passing the --help option should succeed even when pyenv isn't installed.

    pp --help > /dev/null
    [[ $? == 0 ]]
}


test_py_install_error() {
    # The `py install` command should produce an error when pyenv isn't
    # installed.

    pp py install 3.8.4 > /dev/null
    [[ $? != 0 ]]
}


test_py_root_error() {
    # The `py root` command should produce an error when pyenv isn't
    # installed.

    pp py root > /dev/null
    [[ $? != 0 ]]
}


test_env_root() {
    # The `env root` command should succeed event when pyenv isn't installed.

    pp env root > /dev/null
    [[ $? == 0 ]]
}


test_env_list() {
    # The `env list` command should succeed event when pyenv isn't installed.

    pp env list > /dev/null
    [[ $? == 0 ]]
}
