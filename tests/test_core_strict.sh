# Test under the unofficial Bash "strict" mode, in which the `nounset`,
# `errexit`, and `pipefail` options have been enabled. Some users may run pp in
# a script, and a lot of people use strict mode in scripts. direnv (which pp's
# README recommends) has gone back and forth on its use of strict mode, so it
# may be enabled there as well.


setup() {
    source setup_teardowns.sh
    setup_default_location

    source mocks/functions.sh

    set -o nounset -o errexit -o pipefail
}


teardown() {
    teardown_installation_paths
}


source core_tests.sh
