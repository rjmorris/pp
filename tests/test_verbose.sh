# Test the scenario where PP_VERBOSE=1. This isn't intended to be an exhaustive
# test, just checking a few cases where verbose mode should produce more output
# than non-verbose mode.


setup() {
    source setup_teardowns.sh
    setup_default_location

    source mocks/functions.sh
}


teardown() {
    teardown_installation_paths
}


test_py_install() {
    # The `py install` command should produce additional lines of output in
    # verbose mode.

    PP_VERBOSE=0
    nonverbose_lines=$(pp py install 3.8.4 | wc -l)
    pp py uninstall 3.8.4

    PP_VERBOSE=1
    verbose_lines=$(pp py install 3.8.4 | wc -l)

    [[ $verbose_lines > $nonverbose_lines ]]
}


test_py_all() {
    # The `py all` command should produce additional lines of output in verbose
    # mode.

    PP_VERBOSE=0
    nonverbose_lines=$(pp py all | wc -l)

    PP_VERBOSE=1
    verbose_lines=$(pp py all | wc -l)

    [[ $verbose_lines > $nonverbose_lines ]]
}


test_env_delete() {
    # The `env delete` command should produce additional lines of output in
    # verbose mode.

    pp py install 3.8.4
    pp env new testenv 3.8.4

    PP_VERBOSE=0
    nonverbose_lines=$(echo y | pp env delete testenv | wc -l)

    pp env new testenv 3.8.4
    PP_VERBOSE=1
    verbose_lines=$(echo y | pp env delete testenv | wc -l)

    [[ $verbose_lines > $nonverbose_lines ]]
}


test_env_open() {
    # The `env open` command should produce additional lines of output in
    # verbose mode.

    pp py install 3.8.4
    pp env new testenv 3.8.4

    PP_VERBOSE=0
    nonverbose_lines=$(pp env open testenv | wc -l)
    # Opening a virtual environment in a subshell means we don't have to close
    # it manually. Exiting the subshell has an equivalent effect.

    PP_VERBOSE=1
    verbose_lines=$(pp env open testenv | wc -l)

    [[ $verbose_lines > $nonverbose_lines ]]
}


test_env_list() {
    # The `env list` command should produce additional lines of output in
    # verbose mode.

    pp py install 3.8.4
    pp env new testenv 3.8.4

    PP_VERBOSE=0
    nonverbose_lines=$(pp env list | wc -l)

    PP_VERBOSE=1
    verbose_lines=$(pp env list | wc -l)

    [[ $verbose_lines > $nonverbose_lines ]]
}
