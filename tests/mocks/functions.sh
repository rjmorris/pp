pyenv() {
    case "$1" in
        root)
            echo $TESTING_PYENV_ROOT
            ;;
        --version)
            echo pyenv version: stub
            ;;
        install)
            if [[ -z $2 ]]; then
                return 1
            fi
            if [[ $2 == --list ]]; then
                echo Available versions:
                echo 3.6.9
                echo 3.6.10
                echo 3.7.3
                echo 3.7.4
                echo 3.8.4
                echo 3.8.5
                return 0
            fi
            local bin_dir
            bin_dir="$TESTING_PYENV_ROOT/versions/$2/bin"
            mkdir -p "$bin_dir"
            cp mocks/python "$bin_dir/python"
            ;;
        uninstall)
            if [[ -z $2 ]]; then
                return 1
            fi
            rm -rf "$TESTING_PYENV_ROOT/versions/$2"
            ;;
    esac
}
