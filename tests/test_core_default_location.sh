# Test the scenario where pp and pyenv have been installed to their default
# locations, and we haven't configured the pp environment variables like
# PP_ROOT.


setup() {
    source setup_teardowns.sh
    setup_default_location

    source mocks/functions.sh
}


teardown() {
    teardown_installation_paths
}


source core_tests.sh
