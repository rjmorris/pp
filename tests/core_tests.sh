# The test functions in this file are intended to be sourced into a test file
# (that is, a file named `test_*.sh`). The test file should define `setup` and
# `teardown` functions that define the following variables:
#
#   - TESTING_PP_ROOT
#   - TESTING_PP_ENV_ROOT
#   - TESTING_PP_PYTHON_ROOT
#
# as well pulling in our mock functions.
#
# A few conventions for the tests:
#
# - If the test writes to standard output, capture it or redirect it to
#   /dev/null to avoid cluttering up the test suite's output.
#
# - If the purpose of the test is to verify that an error occurs, then the test
#   should be structured something like this:
#
#     error-producing command || return 0
#     return 1
#
#   The idea is to succeed (return 0) if the command fails. Structuring it like
#   this instead:
#
#     error-producing command
#     [[ $? != 0 ]]
#
#   will cause the test to fail when run with the Bash option errexit enabled.
#   That option causes the test function to fail immediately upon executing the
#   error-producing command, so it never reaches the second line.
#
# - If the purpose of the test is to verify that a command's output should
#   contain a certain string, then the test should be structured something like
#   this:
#
#     output-producing command | grep pattern > /dev/null
#     [[ $? == 0 ]]
#
#   It may be tempting to use grep's -q option to avoid redirecting to
#   /dev/null:
#
#     output-producing command | grep -q pattern
#     [[ $? == 0 ]]
#
#   However, this can cause the test to fail when run with the Bash option
#   pipefail. The issue is that the -q option causes grep to close the input end
#   of its pipe and exit successfully as soon as it finds the pattern.
#   Meanwhile, the output-producing command continues writing to the output end
#   of its pipe, but that fails because the pipe is closed. When the pipefail
#   option is enabled, a failure in any part of the pipeline causes the entire
#   pipeline to fail. When the pipefail option is disabled, the output-producing
#   command still fails, but the pipeline's exit status is taken from the last
#   command in the pipeline, in this case the grep that succeeded. (Explanation
#   taken from https://bugzilla.redhat.com/show_bug.cgi?id=1589997.)


test_no_args() {
    # Running with no arguments should produce an error.

    pp > /dev/null || return 0
    return 1
}


test_unknown_command() {
    # Running an unknown command should produce an error.

    pp unknown > /dev/null || return 0
    return 1
}


test_has_help() {
    # Passing the --help option should succeed.

    pp --help > /dev/null
    [[ $? == 0 ]]
}


test_help_has_usage() {
    # Passing the --help option should produce output that includes the string
    # "Usage:".

    pp --help | grep "Usage:" > /dev/null
    [[ $? == 0 ]]
}


test_has_version() {
    # Passing the --version option should succeed.

    pp --version > /dev/null
    [[ $? == 0 ]]
}


test_version_has_output() {
    # Passing the --version option should produce 1 line of output.

    local num_lines
    num_lines=$(pp --version | wc -l)
    [[ $num_lines == 1 ]]
}


test_no_leak() {
    # Running pp shouldn't leak any of its internal variables or functions into
    # the shell environment.

    pp env list > /dev/null

    local leaks=$(set | grep -i '^_pp_')
    [[ -z $leaks ]]
}


test_root() {
    # The `root` command should print the location of the installation
    # directory.

    [[ $(pp root) == $TESTING_PP_ROOT ]]
}


test_py_unknown_command() {
    # Passing an unknown command to `py` should produce an error.

    pp py unknown > /dev/null || return 0
    return 1
}


test_py_install() {
    # The `py install` command should create a directory for the requested
    # Python version.

    pp py install 3.8.4
    [[ -d $TESTING_PP_PYTHON_ROOT/3.8.4 ]]
}


test_py_uninstall() {
    # The `py uninstall` command should remove the directory created by `py
    # install`.

    pp py install 3.8.4
    pp py uninstall 3.8.4
    [[ ! -d $TESTING_PP_PYTHON_ROOT/3.8.4 ]]
}


test_py_uninstall_nonexistent() {
    # The `py uninstall` command should produce an error if the given Python
    # version doesn't exist.

    pp py install 3.8.4
    pp py uninstall 3.8.5 > /dev/null || return 0
    return 1
}


test_py_bin() {
    # The `py bin` command should print the bin directory for the given Python
    # version.

    pp py install 3.8.4
    [[ $(pp py bin 3.8.4) == $TESTING_PP_PYTHON_ROOT/3.8.4/bin ]]
}


test_py_bin_nonexistent() {
    # The `py bin` command should produce an error if the given Python
    # version doesn't exist.

    pp py install 3.8.4
    pp py bin 3.8.5 > /dev/null || return 0
    return 1
}


test_py_list() {
    # The `py list` command should print the installed Python versions.

    pp py install 3.6.10
    pp py install 3.7.3
    pp py install 3.8.4

    [[ $(pp py list | sort | tr "\n" " ") == "3.6.10 3.7.3 3.8.4 " ]]
}


test_py_list_empty() {
    # The `py list` command should produce no output if no Python versions have
    # been installed.

    [[ -z $(pp py list) ]]
}


test_py_all() {
    # The `py all` command should print a list of Python versions that can be
    # installed.
    #
    # This command simply delegates to pyenv and prints whatever pyenv prints,
    # so there isn't much to test. We've mocked pyenv to print some
    # representative output, so we'll at least check for that.

    pp py all | grep "Available versions" > /dev/null
    [[ $? == 0 ]]
}


test_py_root() {
    # The `py root` command should print the location of the Python versions
    # directory.

    [[ $(pp py root) == $TESTING_PP_PYTHON_ROOT ]]
}


test_env_unknown_command() {
    # Passing an unknown command to `env` should produce an error.

    pp env unknown > /dev/null || return 0
    return 1
}


test_env_new() {
    # The `env new` command should create a directory for the given virtual
    # environment.

    pp py install 3.8.4
    pp env new testenv 3.8.4
    [[ -d $(pp env root)/testenv ]]
}


test_env_new_existing() {
    # The `env new` command should produce an error if the given virtual
    # environment already exists.

    pp py install 3.8.4
    pp env new testenv 3.8.4
    pp env new testenv 3.8.4 > /dev/null || return 0
    return 1
}


test_env_new_no_python() {
    # The `env new` command should produce an error if the given Python version
    # isn't installed.

    pp py install 3.8.4
    pp env new testenv 3.8.5 > /dev/null || return 0
    return 1
}


test_env_delete() {
    # The `env delete` command should delete the directory for the given virtual
    # environment.

    pp py install 3.8.4
    pp env new testenv 3.8.4
    echo y | pp env delete testenv > /dev/null
    [[ ! -d $(pp env root)/testenv ]]
}


test_env_delete_nonexistent() {
    # The `env delete` command should produce an error if the given virtual
    # environment doesn't exist.

    pp py install 3.8.4
    pp env new testenv 3.8.4
    pp env delete testenv2 > /dev/null || return 0
    return 1
}


test_env_open() {
    # The `env open` command should run the virtual environment's activate
    # script.
    #
    # We have mocked an activate script that sets the MOCK_ACTIVATE environment
    # variable.

    pp py install 3.8.4
    pp env new testenv 3.8.4
    pp env open testenv
    [[ -n $MOCK_ACTIVATE ]]
}


test_env_open_nonexistent() {
    # The `env open` command should produce an error if the given virtual
    # environment doesn't exist.

    pp py install 3.8.4
    pp env new testenv 3.8.4
    pp env open testenv2 > /dev/null || return 0
    return 1
}


test_env_close() {
    # The `env close` command should run the virtual environment's deactivate
    # script.
    #
    # We have mocked a deactivate script that sets the MOCK_DEACTIVATE
    # environment variable.

    pp py install 3.8.4
    pp env new testenv 3.8.4
    pp env open testenv
    pp env close
    [[ -n $MOCK_DEACTIVATE ]]
}


test_env_close_unopened() {
    # The `env close` command should produce an error if a virtual environment
    # hasn't been opened yet.

    pp py install 3.8.4
    pp env new testenv 3.8.4
    pp env close > /dev/null || return 0
    return 1
}


test_env_bin() {
    # The `env bin` command should print the bin directory for the given virtual
    # environment.

    pp py install 3.8.4
    pp env new testenv 3.8.4
    [[ $(pp env bin testenv) == $TESTING_PP_ENV_ROOT/testenv/bin ]]
}


test_env_bin_nonexistent() {
    # The `env bin` command should produce an error if the given virtual
    # environment doesn't exist.

    pp py install 3.8.4
    pp env new testenv 3.8.4
    pp env bin testenv2 > /dev/null || return 0
    return 1
}


test_env_list() {
    # The `env list` command should print the names of the virtual environments
    # that have been created.

    pp py install 3.8.4
    pp env new testenv1 3.8.4
    pp env new testenv2 3.8.4
    pp env new testenv3 3.8.4

    [[ $(pp env list | sort | tr "\n" " ") == "testenv1 testenv2 testenv3 " ]]
}


test_env_list_empty() {
    # The `env list` command should produce no output if no virtual environments
    # have been created.

    pp py install 3.8.4
    [[ -z $(pp env list) ]]
}


test_env_root() {
    # The `env root` command should print the location of the virtual
    # environment directory.

    [[ $(pp env root) == $TESTING_PP_ENV_ROOT ]]
}
