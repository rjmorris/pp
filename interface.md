# Interface

Which actions should pp support? Below is a list of actions split into rough priority groupings.

Must have:

- Activate a virtual environment. There isn't a convenient way to do this via pyenv without using its shims, so this is the highest priority action.

Nice to have, but can be achieved fairly easily in other ways:

- Create/remove a virtual environment. This can be achieved with `pyenv virtualenv 3.8.4 my-env` and `pyenv uninstall my-env`. 
- List the virtual environments. Assuming we use pyenv to create the virtual environments, this can be achieved with `pyenv list`. However, pyenv lists all the pythons and virtual environments together in one list.
- Deactivate a virtual environment. This can be achieved with the `deactivate` command provided by the virtual environment itself.
- Install/uninstall python. This can be achieved with `pyenv install 3.8.4` and `pyenv uninstall 3.8.4`.
- List the installed pythons. This can be achieved with `pyenv list`. However, if we also use pyenv to create virtual environments, pyenv will list all the pythons and virtual environments together in one list.
- List the pythons available to install. This can be achieved with `pyenv install --list`.
- Edit PATH to add/remove a python or virtual environment. This can be achieved by manually editing PATH. Presumably the user already has (or can easily set up) shell functions for adding a path to PATH or removing a path from PATH.
- Output the path of a python or virtual environment's bin directory. This can be achieved with `$PP_PYTHON_ROOT/3.8.4/bin` or `$PP_ENV_ROOT/my-env/bin`. If pp doesn't support editing PATH, adding a simpler command for this (like `pp bin my-env`) would make manual PATH edits easier.
- Define/undefine the global python or virtual environment. This can be achieved by manually editing PATH (in bashrc for example) to include the desired python or virtual environment's bin directory. This is unlikely to change frequently, so doing it manually shouldn't be much of a burden.

Probably don't need:

- Run a command in a virtual environment. I'm OK with activating, running the command, and deactivating in the rare times I need to do this.
- Copy a virtual environment. I don't know that I've ever needed to do this, and I'm OK with re-creating a new one if necessary. virtualenvwrapper supports this, but it includes a disclaimer that it may not work correctly.

The current set of command names is a work in progress as I assess the names for their ergonomics and memorability. Below are a few other sets of command names I considered that might be worth revisiting.

| Action                   | Current design          | Alternative 1                | Alternative 2          | Alternative 3          |
|--------------------------|-------------------------|------------------------------|------------------------|------------------------|
| Install python           | pp py install 3.8.4     | pp py 3.8.4                  | pp install 3.8.4       | pp py -c 3.8.4         |
| Uninstall python         | pp py uninstall 3.8.4   | pp py --delete 3.8.4         | pp uninstall 3.8.4     | pp py -d 3.8.4         |
| List installable pythons | pp py all               | pp py --all                  | pp all                 | pp py -a               |
| List installed pythons   | pp py list              | pp py --list                 | pp list                | pp py -l               |
| Print python bindir      | pp py bin 3.8.4         | pp py --bin 3.8.4            | pp bin 3.8.4           | pp py -b 3.8.4         |
| Add python to PATH       | n/a                     | pp path 3.8.4                | pp push 3.8.4          | pp py [-p] 3.8.4       |
| Remove python from PATH  | n/a                     | pp path --delete 3.8.4       | pp pop 3.8.4           | pp py -r 3.8.4         |
| Define the global python | n/a                     | pp global 3.8.4              | pp global 3.8.4        | pp py -g 3.8.4         |
| Remove the global python | n/a                     | pp global --delete           | pp global none         | pp py -g none          |
| Create a venv            | pp env new my-env 3.8.4 | pp env --create my-env 3.8.4 | pp create my-env 3.8.4 | pp env -c my-env 3.8.4 |
| Delete a venv            | pp env delete my-env    | pp env --delete my-env       | pp delete my-env       | pp env -d my-env       |
| List created venvs       | pp env list             | pp env --list                | pp list                | pp env -l              |
| Print venv bindir        | pp env bin my-env       | pp env --bin my-env          | pp bin my-env          | pp env -b my-env       |
| Activate a venv          | pp env open my-env      | pp env my-env                | pp use my-env          | pp env my-env          |
| Deactivate the venv      | pp env close            | pp env --deactivate          | pp exit                | pp env -x              |
| Add venv to PATH         | n/a                     | pp path my-env               | pp push my-env         | pp env -p my-env       |
| Remove venv from PATH    | n/a                     | pp path --delete my-env      | pp pop my-env          | pp env -r my-env       |
| Define the global venv   | n/a                     | pp global my-env             | pp global my-env       | pp env -g my-env       |
| Remove the global venv   | n/a                     | pp global --delete           | pp global none         | pp env -g none         |

Some scattered thoughts:

- Most of the actions can viewed as do/undo pairs. For example, you install a python version and uninstall it. You add a python version to the PATH and remove it. You create a virtual environment and delete it. Should each do/undo pair get a unique command name? Or should command names be re-used for similar operations, which would require namespacing (like `pp py install` and `pp env install`)? An advantage of unique command names is that the command can be shorter, because you don't also have to specify the namespace. An advantage of re-using command names is that there are fewer to remember, so you'll have fewer cases of "Is `pp use` for using a Python version or using a virtual environment?" Here are several command name pairs to consider:
    - install/uninstall
    - add/remove
    - create/delete (or destroy)
    - start/stop
    - enter/exit
    - use/??
    - activate/deactivate
    - enable/disable
    - do/undo
    - push/pop
    - open/close
- Instead of setting the "global" Python version, consider framing it as the "default" or "fallback" Python version, and change the command name accordingly.
- In designs that use options (like `--create`), it would also make sense to support a short form (like `-c`). One could make the same argument for the command name as well. Why not support `pp i` as an alias for `pp install`? But really, will you use pp so often that the shorter alias is that important?
- What is the opposite of "use"? Consulting an online thesaurus or two turns up "cease", "discard", "dismiss", "halt", "ignore", "neglect", "reject", "release", "renounce", "stop", "save", and "yield". None of these are particularly satisfying. Maybe "exit", although that pairs more naturally with "enter". Another possibility is "done", even though it isn't a verb.
- Adding a Python version to the PATH is fundamentally different than activating a virtual environment, because multple Python versions can be on the PATH, but only one virtual environment can be activated at a time. Therefore, the command name for activating would ideally imply a sense of exclusivity, while the command name for PATH addition wouldn't. And it follows that the two actions would ideally have different command names.
- Should pp support adding a Python version to the PATH? This operation is pretty easy for someone to do outside of pp, assuming they've defined some sort of path_add function for themselves, which is probably a good idea anyway. Building it into pp would make it a little easier, though, because with pp they'd have to identify only the version number (`pp add 3.8.4`), but outside of pp they'd have to identify the full path to the bin directory (`path_add $PP_PYTHON_ROOT/3.8.4/bin`).
- Should pp support deactivating a virtual environment? As it stands, it simply calls `deactivate`, which the user could do without pp. In theory, if some other virtual environment implementation doesn't use `deactivate`, doing it within pp could provide a standard interface.
- The current design has commands that require specifying a Python version, with the intention that it's a pure Python version, not a virtual environment. For example, in `pp add 3.8.4`, the `3.8.4` argument can't be replaced by a virtual environment name. Other commands like this are `pp create` and `pp global`. Is there any reason to prevent the user from specifying virtual environments for these? `pp create` is a little different than the other two, because even though you can create a virtual environment using another virtual environment (which would look like `pp create my-env1 3.8.2 && pp create my-env2 my-env1` if pp supported it), it's no different than creating the new virtual environment using the Python version that created the first virtual environment (`pp create my-env1 3.8.2 && pp create my-env2 3.8.2`). pyenv has a nice approach here in that it treats pure Python installs and virtual environments as equals, so either could be specified.
