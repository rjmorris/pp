# Why pp?

Many projects provide the same or similar functionality as pp, and I can't claim to have tried all of them before writing pp. I've listed below some of the more popular alternatives along with my reasons for looking elsewhere. Admittedly, some of my gripes are pretty minor, and if someone accused me of developing pp as a symptom of not-invented-here syndrome, I would concede that there is probably some truth to that. And despite this sounding like a laundry list of complaints, I must point out that I think these are all great projects, and I appreciate the effort their authors have made to create, publish, and maintain them. Their approaches just don't match my workflow and preferences 100%, which probably says more about my workflow and preferences than to the projects.

## pyenv + pyenv-virtualenv

I used [pyenv](https://github.com/pyenv/pyenv) and its [pyenv-virtualenv](https://github.com/pyenv/pyenv-virtualenv) plugin for a long time, and I still recommend pyenv for installing multiple versions of Python. However, I eventually became frustrated with pyenv's shim-based design. Although clever, the shims introduced some unexpected behavior. See for example the issue where [shims mask commands installed outside of pyenv](https://github.com/pyenv/pyenv/issues/1112). This is what ultimately prompted me to consider alternatives. The initial motivation of pp was to provide pyenv's core features while avoiding its shim-based design.

## conda

[Conda](https://docs.conda.io/en/latest) handles a lot more than just virtual environment management. I think it can be very useful in some circumstances, but in general it does way more than I need. At the time of writing this, it has 1,500 open issues, indicative of its large surface area. Although you can use pip to install packages from PyPI inside the virtual environments you create, it encourages you to use the Anaconda repository instead, which I'm not interested in. In fact, the docs say:

> Issues may arise when using pip and conda together. When combining conda and pip, it is best to use an isolated conda environment. Only after conda has been used to install as many packages as possible should pip be used to install any remaining software.

I would use conda to create and activate virtual environments, but I wouldn't use it to install any packages. I would use pip to install all my packages, which goes against the recommendation in the docs. I'd rather not commit to a tool when I plan to use it in a way that isn't recommended by its authors.

## virtualenvwrapper

[virtualenvwrapper](https://bitbucket.org/virtualenvwrapper/virtualenvwrapper) is similar in spirit to pp, but my biggest complaint about it is the long and ugly command names. For example, `mkvirtualenv` and `lsvirtualenv` for operations I'd commonly use, and the even uglier `add2virtualenv` and `toggleglobalsitepackages` for operations I probably wouldn't use. And if I'm being really petty, I don't care for the "work on" terminology at all. I just wince every time I type `workon` or see the `WORKON_HOME` variable.

A little more substantively, running `mkvirtualenv` not only creates a virtual environment, as the command name suggests, but also activates it, which the command name doesn't suggest. This violates pp's design goal of being explicit.

virtualenvwrapper also does more than I need. Not as much as conda, but still enough to make me sift through irrelevant (to me) documentation and issues and add mental overhead to my usage. For example, I don't need its support for project management or for commands like the previously-mentioned `toggleglobalsitepackages`.

One last complaint is that all the commands are installed as independent functions in the shell, as opposed to being subcommands within a single command that acts as a namespace. Installing virtualenvwrapper gives you about 20 new commands. I suppose this is pretty minor, but I find the subcommand approach much cleaner, especially for what I consider to be a small utility.

## pew

[pew](https://github.com/berdario/pew) started out as a rewrite of virtualenvwrapper, and it retains some of the things I don't like about virtualenvwrapper, such as the "work on" terminology and automatically activating a virtual environment when you create it.

At the time of writing this, it appears that the pew maintainer has lost interest in the project. There's nothing wrong with that if the project is stable and just works, but there are a number of open issues and pull requests that could be addressed, and I don't want to take a risk on committing to an abandoned project.

pew takes an interesting approach to activating virtual environments: Instead of sourcing the virtual environment's `activate` script, which modifies environment variables in the current shell (modifications which must be reversed when deactivating), it opens a subshell. The environment variables are modified in the subshell, leaving the current shell untouched. Deactivating is then simply a matter of exiting the subshell. It shares this approach with [vex](#vex). Although this violates pp's design goal of using standard tooling (the `activate` script in this case), I do find the argument of avoiding modifications to the current shell compelling.

However, I encountered a big drawback to the subshell approach: I use direnv to automatically activate a virtual environment when entering a directory, and the interaction of direnv and pew produces an infinite loop of new subshells. I could probably find a way to avoid it, but I think it would have to be an unintended use of one of the two tools, and I don't want to go that route.

From a developer's standpoint, pew is appealing because it's written in Python instead of shell script, so it would be more pleasant to maintain. Also, its subshell design means that the user's working shell is irrelevant, so it immediately supports bash, zsh, fish, etc. as working shells. pp would need to be rewritten to support working shells other than bash.

## vex

[vex](https://github.com/sashahart/vex) is similar to [pew](#pew) in that it's written in Python and uses a subshell approach, so it has all the same pros and cons associated with these characteristics, including the infinite loop produced when using it with direnv. Unlike pew, it breaks away from the virtualenvwrapper interface, and I find vex's interface to be more pleasing as a result. I also appreciate that it keeps the number of supported operations small.

vex is designed such that all of its operations involve running a command in a virtual environment's subshell. As a result, if you just want to create a virtual environment, you have to also specify a command to run inside it, or else be placed into a subshell that you then must exit from. Similarly, if you just want to delete a virtual environment, you have to specify a command to run inside it first. I view creating a virtual environment, deleting a virtual environment, activating a virtual environment, and running commands in a virtual environment as independent operations, so being forced to combine them into a single operation is frustrating to me.

Specifying which Python version to use when creating a virtual environment isn't very convenient unless you want to use whichever version is first in your `$PATH`. Otherwise, it must either be later in your `$PATH`, or you must specify the full path to it. (This criticism also applies to pew and virtualenvwrapper.) This is a consequence of vex not managing Python versions, only virtual environments. I do see the value in keeping vex simple by letting other tools manage Python versions, so it's hard to fault that design decision. But in my workflow, I prefer to explicitly identify the Python version to use, and they aren't often in my `$PATH`, so I'd need to enter the full path every time. I like pyenv's approach to this, where you can specify the Python version as simply `3.8.4`.
