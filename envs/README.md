This directory is the default location for storing the virtual environments created by pp.

The primary function of this README is to exist in version control so that git is forced to create the containing directory.
