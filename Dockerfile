FROM debian:buster-slim

RUN useradd --create-home --shell /bin/bash testuser

WORKDIR /code
COPY ./ ./
