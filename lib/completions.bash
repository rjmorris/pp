_pp_complete() {
    # See the "Programmable Completion" section in the Bash docs for a
    # discussion of the requirements. In brief:
    #
    # `COMP_WORDS` and `COMP_CWORD` are provided to us by the Bash completion
    # mechanism. `COMP_WORDS` is an array of the individual words on the current
    # command line. `COMP_CWORD` is an index into `COMP_WORDS` pointing to the
    # word containing the current cursor position on the command line.
    #
    # We're required to assign an array of completions to `COMPREPLY`. The Bash
    # builtin function `compgen` will take care of some of the details for us.

    local incomplete_word=${COMP_WORDS[COMP_CWORD]}
    local completed_words=${COMP_WORDS[@]::COMP_CWORD}

    local completions

    # We're just going to hardcode the completions here in this script. This
    # runs the risk of getting out of sync with the source code, which could be
    # mitigated by adding something like a `--completions` option to every
    # command. However, that seems like a lot of boilerplate to introduce to the
    # source code. This method is very simple by comparison, so I'm willing to
    # take the risk of things getting out of sync.
    case $completed_words in
        pp)
            completions="env py root --help --version"
            ;;
        "pp env")
            completions="bin close delete list new open root"
            ;;
        "pp py")
            completions="all bin install list root uninstall"
            ;;
        "pp env bin"|"pp env delete"|"pp env open")
            completions=$(pp env list)
            ;;
        "pp env new "*|"pp py bin"|"pp py uninstall")
            completions=$(pp py list)
            ;;
        "pp py install")
            completions=$(pp py all | grep -v Available)
            ;;
        *)
            COMPREPLY=()
            return 0
            ;;
    esac

    COMPREPLY=($(compgen -W "$completions" -- $incomplete_word))
    return 0
}

# Use the Bash builtin function `complete` to assign our completion function to
# the `pp` command.
complete -F _pp_complete pp
