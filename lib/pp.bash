pp() {
    local _PP_VERSION=1.0.0-rc2

    # Assign values to the configuration variables.
    #
    # Most of them are straightforward, but the default for _PP_PYTHON_ROOT
    # comes from an external command (pyenv), so we handle that one differently.
    # We first give it an empty default and then assign it inside a function. By
    # assigning it inside a function, we can call the function only for the
    # operations where the variable is actually needed. In cases where that
    # external command isn't available for some reason, this allows us to
    # perform other operations without error. For example, if pyenv isn't
    # installed, we can still run `pp --help` using this setup.

    local _PP_ROOT
    local _PP_ENV_ROOT
    local _PP_PYTHON_ROOT
    local _PP_VERBOSE

    _PP_ROOT=${PP_ROOT:-$HOME/.pp}
    _PP_ENV_ROOT=${PP_ENV_ROOT:-$_PP_ROOT/envs}
    _PP_PYTHON_ROOT=${PP_PYTHON_ROOT:-}
    _PP_VERBOSE=${PP_VERBOSE:-0}

    _pp_assign_python_root() {
        if [[ -z $_PP_PYTHON_ROOT ]]; then
            if _pp_assert_pyenv_installed; then
                _PP_PYTHON_ROOT=$(pyenv root)/versions
            else
                _pp_error "Unable to assign a path to PP_PYTHON_ROOT. Please install pyenv or configure the PP_PYTHON_ROOT variable."
                return 1
            fi
        fi
        return 0
    }


    _pp_help() {
        echo 'pp: Manage Python versions and virtual environments'
        echo
        echo 'Usage:'
        echo '  pp <command> [arguments]'
        echo '  pp [-h|--help]'
        echo '  pp [-v|--version]'
        echo
        echo 'Commands:'
        echo '  env new - Create a new virtual environment.'
        echo '  env delete - Delete a virtual environment.'
        echo '  env open - Activate a virtual environment.'
        echo '  env close - Deactivate the currently active virtual environment.'
        echo "  env bin - Print the path to a virtual environment's bin directory."
        echo '  env list - List the installed virtual environments.'
        echo '  env root - Print the path to the directory containing virtual environments.'
        echo '  py install - Install a Python version.'
        echo '  py uninstall - Uninstall a Python version.'
        echo "  py bin - Print the path to a Python version's bin directory."
        echo '  py list - List the installed Python versions.'
        echo '  py all - List the Python versions available for installation.'
        echo '  py root - Print the path to the directory containing Python versions.'
        echo '  root - Print the path to the pp installation directory.'
        echo 'Run pp <command> --help for help on a specific command.'
        echo
        echo 'Environment variables:'
        echo '  PP_ROOT - pp installation directory.'
        echo '    default value: ~/.pp'
        echo '  PP_PYTHON_ROOT - Directory containing python versions.'
        echo '    default value: $(pyenv root)/versions'
        echo '  PP_ENV_ROOT - Directory containing virtual environments.'
        echo '    default value: $(pp root)/envs'
        echo '  PP_VERBOSE - Set to 1 for verbose status messages.'
        echo '    default value: 0'
        echo
        echo "Version: $_PP_VERSION"
    }


    _pp_main() {
        if [[ $# == 0 ]]; then
            _pp_usage_error "no command provided"
            return 1
        fi

        local command=$1
        shift

        case $command in
            --help|-h)
                _pp_help
                ;;
            --version|-v)
                _pp_version
                ;;
            py)
                local subcommand=$1
                shift
                case $subcommand in
                    install)
                        _pp_py_install "$@"
                        ;;
                    uninstall)
                        _pp_py_uninstall "$@"
                        ;;
                    bin)
                        _pp_py_bin "$@"
                        ;;
                    list)
                        _pp_py_list "$@"
                        ;;
                    root)
                        _pp_py_root "$@"
                        ;;
                    all)
                        _pp_py_all "$@"
                        ;;
                    *)
                        _pp_usage_error "Unexpected command: $command $subcommand"
                        return 1
                        ;;
                esac
                ;;
            env)
                local subcommand=$1
                shift
                case $subcommand in
                    new)
                        _pp_env_new "$@"
                        ;;
                    delete)
                        _pp_env_delete "$@"
                        ;;
                    open)
                        _pp_env_open "$@"
                        ;;
                    close)
                        _pp_env_close "$@"
                        ;;
                    bin)
                        _pp_env_bin "$@"
                        ;;
                    list)
                        _pp_env_list "$@"
                        ;;
                    root)
                        _pp_env_root "$@"
                        ;;
                    *)
                        _pp_usage_error "Unexpected command: $command $subcommand"
                        return 1
                        ;;
                esac
                ;;
            root)
                _pp_root "$@"
                ;;
            *)
                _pp_usage_error "Unexpected command: $command"
                return 1
                ;;
        esac
    }


    _pp_version() {
        echo "$_PP_VERSION"
    }


    _pp_print() {
        echo "pp: $1"
    }


    _pp_verbose() {
        if [[ $_PP_VERBOSE == 1 ]]; then
            _pp_print "$1"
        fi
    }


    _pp_error() {
        echo "pp error: $1"
    }


    _pp_usage_error() {
        local cmd
        local msg
        if [[ $# == 2 ]]; then
           cmd=$1
           msg=$2
        else
           msg=$1
           cmd=""
        fi
        _pp_error "$msg"
        echo "View usage instructions with: pp $cmd --help"
    }


    _pp_assert_pyenv_installed() {
        if ! pyenv --version > /dev/null 2>&1; then
            _pp_error "pyenv doesn't appear to be installed"
            return 1
        fi
        return 0
    }


    _pp_assert_deactivate_installed() {
        if ! type deactivate > /dev/null 2>&1; then
            _pp_error "deactivate doesn't appear to be installed"
            return 1
        fi
        return 0
    }


    _pp_assert_root_dir_exists() {
        if [[ ! -d $_PP_ROOT ]]; then
            _pp_error "PP_ROOT is not a directory: PP_ROOT=$_PP_ROOT"
            return 1
        fi
        return 0
    }


    _pp_assert_env_root_dir_exists() {
        if [[ ! -d $_PP_ENV_ROOT ]]; then
            _pp_error "PP_ENV_ROOT is not a directory: PP_ENV_ROOT=$_PP_ENV_ROOT"
            return 1
        fi
        return 0
    }


    _pp_assert_python_root_dir_exists() {
        _pp_assign_python_root || return 1

        if [[ ! -d $_PP_PYTHON_ROOT ]]; then
            _pp_error "PP_PYTHON_ROOT is not a directory: PP_PYTHON_ROOT=$_PP_PYTHON_ROOT"
            return 1
        fi
        return 0
    }


    _pp_assert_python_version_installed() {
        _pp_assert_python_root_dir_exists || return 1

        local python_version=$1
        local python_dir="$_PP_PYTHON_ROOT/$python_version"
        if [[ ! -d $python_dir ]]; then
            _pp_error "No python version $python_version found in PP_PYTHON_ROOT=$_PP_PYTHON_ROOT"
            return 1
        fi
        return 0
    }


    _pp_assert_env_installed() {
        _pp_assert_env_root_dir_exists || return 1

        local env_name=$1
        local env_dir="$_PP_ENV_ROOT/$env_name"
        if [[ ! -d $env_dir ]]; then
            _pp_error "No virtual environment named $env_name found in PP_ENV_ROOT=$_PP_ENV_ROOT"
            return 1
        fi
        return 0
    }


    _pp_py_install() {
        if [[ $# == 1 && $1 == "--help" ]]; then
            echo 'Install a Python version.'
            echo 'Usage: pp py install <python_version> [options]'
            echo
            echo 'pp delegates the installation task to pyenv. Any options'
            echo 'provided to this command are passed unmodified to the'
            echo '`pyenv install` command. Please note that the options'
            echo 'MUST be provided AFTER the Python version in this command.'
            return 0
        fi
        if [[ $# == 0 ]]; then
            _pp_usage_error "py install" "incorrect number of arguments"
            return 1
        fi
        local python_version=$1
        shift

        _pp_assert_pyenv_installed || return 1

        _pp_verbose "installing Python version $python_version using pyenv"
        pyenv install "$@" $python_version
        return $?
    }


    _pp_py_uninstall() {
        if [[ $# == 1 && $1 == "--help" ]]; then
            echo 'Uninstall a Python version.'
            echo 'Usage: pp py uninstall <python_version> [options]'
            echo
            echo 'pp delegates the uninstallation task to pyenv. Any options'
            echo 'provided to this command are passed unmodified to the'
            echo '`pyenv uninstall` command. Please note that the options'
            echo 'MUST be provided AFTER the Python version in this command.'
            return 0
        fi
        if [[ $# == 0 ]]; then
            _pp_usage_error "py uninstall" "incorrect number of arguments"
            return 1
        fi
        local python_version=$1
        shift

        _pp_assert_pyenv_installed || return 1
        _pp_assert_python_version_installed $python_version || return 1

        _pp_verbose "uninstalling Python version $python_version using pyenv"
        pyenv uninstall "$@" $python_version
        return $?
    }


    _pp_env_new() {
        if [[ $# == 1 && $1 == "--help" ]]; then
            echo 'Create a new virtual environment.'
            echo 'Usage: pp env new <venv_name> <python_version>'
            return 0
        fi
        if [[ $# != 2 ]]; then
            _pp_usage_error "env new" "incorrect number of arguments"
            return 1
        fi
        local env_name=$1
        local python_version=$2

        _pp_assert_python_version_installed $python_version || return 1
        _pp_assert_env_root_dir_exists || return 1

        local python_exe="$(pp py bin $python_version)/python"
        local env_dir="$_PP_ENV_ROOT/$env_name"

        if [[ -d $env_dir ]]; then
            _pp_error "virtual environment $env_name already exists"
            return 1
        fi

        _pp_verbose "creating virtual environment $env_name in $_PP_ENV_ROOT using python $python_version"
        if ! "$python_exe" -m venv "$env_dir"; then
            echo "pp: failed to create virtual environment $env_name"
            return 1
        fi
        _pp_verbose "virtual environment $env_name created"
        _pp_verbose "activate the virtual environment with: pp env open $env_name"
        return 0
    }


    _pp_env_delete() {
        if [[ $# == 1 && $1 == "--help" ]]; then
            echo 'Delete a virtual environment.'
            echo 'Usage: pp env delete <venv_name>'
            return 0
        fi
        if [[ $# != 1 ]]; then
            _pp_usage_error "env delete" "incorrect number of arguments"
            return 1
        fi
        local env_name=$1

        _pp_assert_env_root_dir_exists || return 1
        _pp_assert_env_installed $env_name || return 1

        _pp_print "deleting virtual environment $env_name located in $_PP_ENV_ROOT"
        read -r -p "Are you sure? (y/N) "
        if [[ $REPLY == "Y" || $REPLY == "y" ]]; then
            if ! rm -rf "$_PP_ENV_ROOT/$env_name"; then
                _pp_error "failed to delete virtual environment $env_name"
                return 1
            fi
            _pp_verbose "deleted virtual environment $env_name"
        else
            _pp_verbose 'deletion canceled'
        fi
        return 0
    }


    _pp_env_open() {
        if [[ $# == 1 && $1 == "--help" ]]; then
            echo 'Activate a virtual environment.'
            echo 'Usage: pp env open <venv_name>'
            return 0
        fi
        if [[ $# != 1 ]]; then
            _pp_usage_error "env open" "incorrect number of arguments"
            return 1
        fi
        local env_name=$1

        _pp_assert_env_root_dir_exists || return 1
        _pp_assert_env_installed $env_name || return 1

        _pp_verbose "activating virtual environment $env_name located in $_PP_ENV_ROOT"
        if ! source "$(pp env bin $env_name)/activate"; then
            _pp_error "failed to activate virtual environment $env_name"
            return 1
        fi
        _pp_verbose 'deactivate the virtual environment with: pp env close'
        return 0
    }


    _pp_env_close() {
        if [[ $# == 1 && $1 == "--help" ]]; then
            echo 'Deactivate the currently active virtual environment.'
            echo 'Usage: pp env close'
            return 0
        fi
        if [[ $# != 0 ]]; then
            _pp_usage_error "env close" "incorrect number of arguments"
            return 1
        fi

        if ! _pp_assert_deactivate_installed > /dev/null; then
            _pp_error 'no virtual environment is open'
            return 1
        fi

        _pp_verbose 'deactivating the current virtual environment'
        if ! deactivate; then
            _pp_error 'failed to deactivate the current virtual environment'
            return 1
        fi
        return 0
    }


    _pp_py_bin() {
        if [[ $# == 1 && $1 == "--help" ]]; then
            echo "Print the path to a Python version's bin directory."
            echo 'Usage: pp py bin <venv_name>'
            return 0
        fi
        if [[ $# != 1 ]]; then
            _pp_usage_error "py bin" "incorrect number of arguments"
            return 1
        fi
        local python_version=$1

        _pp_assert_python_root_dir_exists || return 1
        _pp_assert_python_version_installed $python_version || return 1

        local bin_dir="$_PP_PYTHON_ROOT/$python_version/bin"
        echo "$bin_dir"
        if [[ ! -d $bin_dir ]]; then
            _pp_error "$bin_dir is not a directory"
            return 1
        fi
        return 0
    }


    _pp_env_bin() {
        if [[ $# == 1 && $1 == "--help" ]]; then
            echo "Print the path to a virtual environment's bin directory."
            echo 'Usage: pp env bin <venv_name>'
            return 0
        fi
        if [[ $# != 1 ]]; then
            _pp_usage_error "env bin" "incorrect number of arguments"
            return 1
        fi
        local env_name=$1

        _pp_assert_env_root_dir_exists || return 1
        _pp_assert_env_installed $env_name || return 1

        local bin_dir="$_PP_ENV_ROOT/$env_name/bin"
        echo "$bin_dir"
        if [[ ! -d $bin_dir ]]; then
            _pp_error "$bin_dir is not a directory"
            return 1
        fi
        return 0
    }


    _pp_py_list() {
        if [[ $# == 1 && $1 == "--help" ]]; then
            echo 'List the installed Python versions.'
            echo 'Usage: pp py list'
            return 0
        fi
        if [[ $# != 0 ]]; then
            _pp_usage_error "py list" "incorrect number of arguments"
            return 1
        fi

        _pp_assert_python_root_dir_exists || return 1

        local child

        _pp_verbose "Python version directory: $_PP_PYTHON_ROOT"
        _pp_verbose 'Installed Python versions:'
        for child in "$_PP_PYTHON_ROOT"/*; do
            if [[ ! -L $child && -d $child ]]; then
                local name=$(basename $child)
                echo "$name"
            fi
        done

        return 0
    }


    _pp_env_list() {
        if [[ $# == 1 && $1 == "--help" ]]; then
            echo 'List the installed virtual environments.'
            echo 'Usage: pp env list'
            return 0
        fi
        if [[ $# != 0 ]]; then
            _pp_usage_error "env list" "incorrect number of arguments"
            return 1
        fi

        _pp_assert_env_root_dir_exists || return 1

        local child

        _pp_verbose "Virtual environment directory: $_PP_ENV_ROOT"
        _pp_verbose 'Installed virtual environments:'
        for child in "$_PP_ENV_ROOT"/*; do
            if [[ -d $child ]]; then
                local name=$(basename $child)
                echo "$name"
            fi
        done

        return 0
    }


    _pp_py_all() {
        if [[ $# == 1 && $1 == "--help" ]]; then
            echo 'List the Python versions available for installation.'
            echo 'Usage: pp py all'
            echo
            echo 'pp delegates this task to pyenv by running `pyenv install --list`.'
            return 0
        fi
        if [[ $# != 0 ]]; then
            _pp_usage_error "py all" "incorrect number of arguments"
            return 1
        fi

        _pp_assert_pyenv_installed || return 1

        _pp_verbose 'using pyenv to list all Python versions available for installation'
        pyenv install --list
        return $?
    }


    _pp_root() {
        if [[ $# == 1 && $1 == "--help" ]]; then
            echo 'Print the path to the pp installation directory.'
            echo 'Usage: pp root'
            return 0
        fi
        if [[ $# != 0 ]]; then
            _pp_usage_error "root" "incorrect number of arguments"
            return 1
        fi

        # Don't assert that _PP_ROOT exists first. We want to display the
        # path even if it doesn't exist.

        echo $_PP_ROOT
        return 0
    }


    _pp_py_root() {
        if [[ $# == 1 && $1 == "--help" ]]; then
            echo 'Print the path to the directory containing Python versions.'
            echo 'Usage: pp py root'
            return 0
        fi
        if [[ $# != 0 ]]; then
            _pp_usage_error "py root" "incorrect number of arguments"
            return 1
        fi

        # Don't assert that _PP_PYTHON_ROOT exists first. We want to display the
        # path even if it doesn't exist.

        _pp_assign_python_root || return 1

        echo $_PP_PYTHON_ROOT
        return 0
    }


    _pp_env_root() {
        if [[ $# == 1 && $1 == "--help" ]]; then
            echo 'Print the path to the directory containing virtual environments.'
            echo 'Usage: pp env root'
            return 0
        fi
        if [[ $# != 0 ]]; then
            _pp_usage_error "env root" "incorrect number of arguments"
            return 1
        fi

        # Don't assert that _PP_ENV_ROOT exists first. We want to display the
        # path even if it doesn't exist.

        echo $_PP_ENV_ROOT
        return 0
    }


    _pp_main "$@"
    exit_code=$?


    # Clean up after ourselves so we don't leave a lot of functions defined in
    # the user's shell.
    #
    # Bash doesn't support local functions, so even though we've defined all the
    # helper functions inside the main pp() function, all of them are placed in
    # the global shell environment. This is why we still want to keep the _pp_
    # prefix in their names. Otherwise we run a bigger risk of overwriting an
    # existing function with the same name.
    unset -f _pp_assert_deactivate_installed
    unset -f _pp_assert_env_root_dir_exists
    unset -f _pp_assert_env_installed
    unset -f _pp_assert_pyenv_installed
    unset -f _pp_assert_python_root_dir_exists
    unset -f _pp_assert_python_version_installed
    unset -f _pp_assert_root_dir_exists
    unset -f _pp_assign_python_root
    unset -f _pp_env_bin
    unset -f _pp_env_close
    unset -f _pp_env_delete
    unset -f _pp_env_list
    unset -f _pp_env_new
    unset -f _pp_env_open
    unset -f _pp_env_root
    unset -f _pp_error
    unset -f _pp_help
    unset -f _pp_init_global_vars
    unset -f _pp_main
    unset -f _pp_print
    unset -f _pp_py_all
    unset -f _pp_py_bin
    unset -f _pp_py_install
    unset -f _pp_py_list
    unset -f _pp_py_root
    unset -f _pp_py_uninstall
    unset -f _pp_root
    unset -f _pp_usage_error
    unset -f _pp_verbose
    unset -f _pp_version


    return $exit_code
}
