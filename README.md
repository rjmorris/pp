# pp (Python Python!)

pp is a Bash function for managing multiple Python versions and virtual environments. It's designed to be used in conjunction with [pyenv](https://github.com/pyenv/pyenv). It works by wrapping some of pyenv's operations and replacing others, seeking to provide the most useful features from pyenv but without using pyenv's shim system.

pp offers the following features:

- Install multiple versions of Python (by delegating to pyenv).
- Create a virtual environment based on any of the installed Python versions.
- Activate a virtual environment.
- List the Python versions you've installed and the virtual environments you've created.
- Print the path to the `bin` directory where a virtual environment's or Python version's `python` command is located.

You may wonder why pp exists when other tools providing these features are already available, such as pyenv, conda, and virtualenv. See [Why pp?](./why.md) for a discussion of pp compared to other tools.

## Installing pp

The recommended installation method is to clone the `release` branch of the repo somewhere on your hard drive. The defaults (and the remaining instructions) assume you've cloned it to `~/.pp`:

```
git clone --branch release --single-branch https://gitlab.com/rjmorris/pp.git ~/.pp
```

Add the following to your `~.bashrc`:

```
source ~/.pp/lib/pp.bash
source ~/.pp/lib/completions.bash
export -f pp
```

The last two lines are optional. Sourcing the completions file enables Bash completions for pp, allowing you to complete the names of commands and virtual environments with the tab key. Exporting the `pp` function makes it available when running scripts, which can be useful in some circumstances, such as [using pp with direnv](#pyenv-local).

Because pp relies on [pyenv](https://github.com/pyenv/pyenv) for some of its operations, you should also install pyenv. The pyenv installation instructions include several steps. Most of the steps are important, but you can omit one of them: adding `eval "$(pyenv init -)"` to your `~/.bashrc`. That step sets up pyenv's shim system, which isn't used by pp. (If you're fine with pyenv's shim system, you should probably just use pyenv instead of pp. Replacing pyenv's shim system was the primary motivator for writing pp in the first place.) It also sets up pyenv's Bash completions. If you don't plan to use pyenv directly, then you also won't need its completions. But in case you do want its completions without setting up its shim system, add `source $(pyenv root)/completions/pyenv.bash` to your `~/.bashrc`.

See the [Customization](./customization) section if you want to modify any of pp's settings.

To upgrade pp, run `git pull` in the directory where you cloned the repo.

## Using pp

pp makes a distinction between operations performed on Python versions and operations performed on virtual environments. Commands related to Python versions fall under the `py` namespace, so they take the form `pp py <cmd>`. Commands related to virtual environments fall under the `env` namespace, so they take the form `pp env <cmd>`.

Install and uninstall multiple versions of Python (by delegating to pyenv):

```
$ pp py install 3.7.3
$ pp py install 3.8.2
$ pp py uninstall 3.7.3
```

Create and delete virtual environments based on an installed version of Python:

```
$ pp env new my-env 3.8.2
$ pp env new my-env-3.7 3.7.3
$ pp env delete my-env
```

Activate and deactivate virtual environments:

```
$ pp env open my-env1
$ pp env open my-env2
$ pp env close
```

Print the path to a Python version's `bin` directory:

```
$ pp py bin 3.8.4
/home/username/.pyenv/versions/3.8.4/bin
```

Print the path to a virtual environment's `bin` directory:

```
$ pp env bin my-env
/home/username/.pp/envs/my-env/bin
```

Print the currently installed virtual environments:

```
$ pp env list
my-env1
my-env2
```

Print the currently installed Python versions:

```
$ pp py list
3.6.4
3.7.3
3.8.2
```

Print the list of all Python versions available for installation (by delegating to pyenv):

```
$ pp py all
Available versions:
  2.1.3
  2.2.3
  2.3.7
  ...
```

Print the directories where components are installed:

```
$ pp root
/home/username/.pp
$ pp py root
/home/username/.pyenv/versions
$ pp env root
/home/username/.pp/envs
```

Print help or version info:

```
$ pp --help
$ pp env new --help
$ pp py bin --help
$ pp --version
```

## Replacing pyenv features

Running pyenv without its shim system or without the pyenv-virtualenv plugin will disable many of its features. This section describes how you can replace those missing features using other methods.

### pyenv shell

The `pyenv shell` command assigns a Python version or virtual environment to the current shell. Subsequent `python` commands will run the one you assigned to the shell. The same behavior can be accomplished with pp using a different approach depending on whether you want to use a Python version or a virtual environment.

For a Python version, the pyenv behavior is equivalent to adding the desired Python to `$PATH`. You can do this with `export PATH=$(pp py bin 3.8.4):$PATH`.

For a virtual environment, one approach is to add it to `$PATH`, just like you would for a Python version, with `export PATH=$(pp env bin my-env):$PATH`. Alternatively, you could activate the virtual environment with `pp env open my-env`.

### pyenv local

The `pyenv local` command assigns a Python version or virtual environment to a directory. Subsequent `python` commands issued inside the directory will run the one you assigned to the directory. pp doesn't provide this behavior. Instead, I recommend using [direnv](https://direnv.net) for any directory-specific configuration you want to perform.

One nice advantage of direnv over pyenv for this is that you aren't limited to Python configuration. You can also use it for non-Python projects, and you can use it to perform additional actions such as setting project-specific environment variables.

The behavior provided by `pyenv local` is equivalent to adding the desired Python version or virtual environment to your `$PATH`. This can be achieved with direnv using its `PATH_add` command. For example, to use Python 3.8.4, navigate to the directory of interest, run `direnv edit .` to open the `.envrc` file, and add:

```
PATH_add $(pp py bin 3.8.4)
```

Now, whenever you navigate to that directory or one of its subdirectories, direnv will add Python 3.8.4 to `$PATH` for you, and when you navigate away, direnv will remove Python 3.8.4 from `$PATH` for you.

Similarly, to use the `python` available in the `my-env` virtual environment, add this to `.envrc` instead:

```
PATH_add $(pp env bin my-env)
```

The examples above assume your `~/.bashrc` contains the line `export -f pp` (as mentioned in the [installation section](#installing-pp), which makes it easier to use pp with direnv. However, if you decided not to add `export -f pp` to your `~/.bashrc`, then you'll need to import pp into your direnv config before being able to use it. To do that, you could add the following to the top of every `.envrc`:

```
source ~/.pp/lib/pp.bash
```

Or, you might prefer to add that line to `~/.config/direnv/direnvrc`, and then you won't need it in each `.envrc`.

### pyenv global

The `pyenv global` command assigns a Python version or virtual environment to use as the "global" Python. This Python will be available at all times (although it can be overridden by a `pyenv local` or `pyenv shell` command). pp doesn't include a replacement for `pyenv global`, but it's pretty easy to manage this yourself using symbolic links and the `$PATH` variable. For example, first create a symbolic link to the desired Python version or virtual environment:

```
$ ln -s "$(pp py root)/3.8.4" ~/.local/default_python
```

Then edit your `~/.bashrc` to add the `default_python` location to your `$PATH`:

```
export PATH=~/.local/default_python/bin:$PATH
```

Later, if you decide you want to use a different Python by default, you only need to change the symbolic link:

```
$ ln -s "$(pp env root)/my-env" ~/.local/default_python
```

### Assigning fallback Python versions or virtual environments

For any of `pyenv shell`, `pyenv local`, or `pyenv global`, you can specify multiple Python versions or virtual environments in the order you want them searched for commands. For example, if you do `pyenv local my-env-1 my-env-2 3.7.3 3.6.10`, then any time you issue a command, pyenv will first check to see if it's available in the `my-env-1` virtual environment. If so, then it runs the version of the command that appears in `my-env-1`. If not, then it checks the `my-env-2` virtual environment, and so on. Adding Python versions to the end, like `3.7.3 3.6.10`, allows you to do things like test your code in multiple versions of Python by running it with `python3.7 app.py` and `python3.6 app.py`.

pp doesn't provide this feature, but you can accomplish it by manipulating `$PATH`. The equivalent of the example mentioned above, that is, `pyenv local my-env-1 my-env-2 3.7.3 3.6.10`, would look like:

```
$ export PATH=$(pp py bin 3.6.10):$PATH
$ export PATH=$(pp py bin 3.7.3):$PATH
$ export PATH=$(pp env bin my-env-2):$PATH
$ export PATH=$(pp env bin my-env-1):$PATH
```

Or if you want to put them all on one line:

```
$ export PATH=$(pp env bin my-env-1):$(pp env bin my-env-1):$(pp py bin 3.7.3):$(pp py bin 3.6.10):$PATH
```

This is clearly a lot more typing than the pyenv equivalent. However, if you plan to do this often, and it's tied to a project directory, I highly recommend using direnv (as described earlier in the [replacing pyenv local](#pyenv-local) section) and setting up `$PATH` in your `.envrc`. Note that direnv provides a `PATH_add` function, so the `.envrc` would look something like this:

```
PATH_add $(pp py bin 3.6.10)
PATH_add $(pp py bin 3.7.3)
PATH_add $(pp env bin my-env-2)
PATH_add $(pp env bin my-env-1)
```

### Automatically activating a virtual environment inside a directory

If you install the [pyenv-virtualenv](https://github.com/pyenv/pyenv-virtualenv) plugin along with pyenv, you can set it up to automatically activate a virtual environment when navigating to a directory. pp doesn't provide this feature, but it can be achieved with direnv. For example, to activate the `my-env` virtual environment whenever you enter a given directory, navigate to the directory of interest, run `direnv edit .` to open the `.envrc` file, and add:

```
pp env open my-env
```

Now, any time you enter the directory containing this `.envrc`, the `my-env` virtual environment will be activated for you. When you leave the directory, the virtual environment will be deactivated.

## Customizing pp

You can set environment variables to customize pp. The following variables are available:

- `PP_ROOT` - Directory where pp was installed. Default: `~/.pp`.
- `PP_ENV_ROOT` - Directory where virtual environments will be stored. Default: `$PP_ROOT/envs`
- `PP_PYTHON_ROOT` - Directory where Python versions will be stored. The default location is `$(pyenv root)/versions`.
- `PP_VERBOSE` - Whether pp should display status messages in response to each command. Set to 1 to enable status messages or 0 to disable. Default: 0.

Your `~/.bashrc` or `~/.bash_profile` is probably the best place to define these.

## How pp works

pp is implemented as a series of bash functions that set environment variables and add/remove files and directories. The main entrypoint is a function named `pp`, and it in turn calls helper functions. All the helper functions have names starting with a `_pp_` prefix, so hopefully you won't have any name conflicts with other functions installed in your shell.

For installing and uninstalling Python, pp delegates to pyenv. pyenv installs the various Python versions to `$(pyenv root)/versions`, and pp accesses that location.

For creating a virtual environment, pp runs `python -m venv <env_name>` using the Python version you specify and stores the virtual environment in `$(pp env root)`. Support for `-m venv` was added in Python 3.3, so pp doesn't currently support creating virtual environments using earlier versions of Python.

For activating a virtual environment, pp runs the virtual environment's `activate` script.

## Design goals

pp has the following design goals:

- Minimize the number of supported operations. Support the essential operations for managing virtual environments and nothing more. (Because Python versions themselves are essential to virtual environments, managing multiple Python versions is in scope although reasonable to delegate to other tools.)
- Make the user be explicit. Don't try to guess the user's intention, and don't try to be "helpful" or "smart". For example, if the user asks to activate a virtual environment, don't first check to see whether the virtual environment exists and create it if necessary; instead provide an error message and exit.
- Use standard tooling as much as possible. Be a thin wrapper around the tools one would ordinarily use to perform the operations. For example, to activate a virtual environment, pp runs the virtual environment's `activate` script instead of trying to re-implement the behavior of `activate`. By wrapping instead of re-implementing, there's a good chance you can still do something yourself using standard tools if pp doesn't support it directly.
